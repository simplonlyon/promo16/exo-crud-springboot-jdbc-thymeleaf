## OBJECTIF : CREER UN CRUD : GERER DES ABONNES ##

### SPRING BOOT - SPRING DATA JDBC - MYSQL DRIVER - THYMELEAF ###

## How to use ##
Cloner le projet
Créer un projet Spring Boot directement sur Visual Studio Code
Ajouter l'extension Spring Boot Extension Pack dans Visual Studio Code

Ctrl + Shift + P
Taper spring
Sélectionner Spring Initializer:Create a Maven Project
Sélectionner Version de Spring Boot : choisir la dernière version stable (éviter les versions SNAPSHOT)
Sélectionner le langage du projet : java
Préciser le Group (package name) : com.simplon
Préciser l'Artifact (nom du projet): springboot-jdbc-thymeleaf
Sélectionner le type du Packaging : Jar
Sélectionner la version de Java : 17
Cocher les dépendances : 
- Spring Web
- Spring Boot DevTools

// Lancer l'application :
- directement du terminal : ./mvnw spring-boot:run
- à partir du fichier SpringbootjdbcthymeleafApplication.java (cliquer sur Run)

// Tester son application
Se rendre sur : http://localhost:8080 
(// affiche Whitelabel Error Page)

### Exercice 0 : KANBAN ###
Organiser son travail en suivant la méthode KANBAN (TO DO - DOING - DONE).
Les exercices correspondent à des sprints.
Utiliser l'outil Trello si vous le souhaitez : https://trello.com/fr

### Exercice 0 : GITLAB ###
Créer un dépôt distant sur GitLab
Initialiser son projet avec Git (git init)
Versionner son code : git add . + git commit -m "project init"
Se connecter au dépôt distant : git add remote ...
Déposer son code sur le dépôt distant : git push origin master
Penser à versionner son code après chaque exercice

### Exercice 1 : UML ###
Créer un diagramme de classe représentant la classe Subscriber

Propriétés (private) :
- subscriberId
- firstName
- lastName
- email
- createdAt

Méthodes (public) :
- List findAll();
- int add(Subscriber subscriber);
- Subscriber getSubscriberById(Integer subscriberId);
- void updateSubscriberById(String newFname, String newLname, String newEmail, Integer subscriberId)
- void deleteSubscriberById(Integer subscriberId)

### Exercice 2 : SQL ###
Créer un fichier springboot-jdbc-thymeleaf.sql

Créer une base de données si elle n'existe pas nommée springboot-jdbc-thymeleaf
Utiliser cette base de données pour créer la table suivante

Supprimer la table subscribers si elle existe
Créer la table subscribers :
- subscriberId de type INTEGER, clé primaire, généré automatiquement
- firstName de type VARCHAR(255), non null
- lastName de type VARCHAR(255), non null
- email de type VARCHAR(255), non null, clé unique
- createdAt de type TIMESTAMP, non null, par défault CURRENT_TIMESTAMP
- Choisir InnoDB pour l'ENGINE, par défaut utf8 pour le charset

Insérer des données dans la table (uniquement les valeurs des champs firstName, lastName, email)

### Exercice 3 : MODEL ### 
Créer un package model
Créer une entity Subscriber (subscriberId, firstName, lastName, email, createdAt)

### Exercice 4 : DAO ### 
Créer un package dao
Créer une interface SubscriberDAO
Implémenter la signature de méthode findAll() et faire en sorte qu'elle retourne un objet de type List

### Exercice 5 : DAO ### 
Créer une classe SubscriberDAOImpl qui implémente SubscriberDAO
La précéder de l'annotation @Repository
Injecter l'objet Datasource datasource grâce à l'annotation @Autowired
Implémenter la méthode findAll() :
- Déclarer un objet Connection cnx
- Déclarer un objet List et l'initialiser avec une new ArrayList<>()
- Utiliser un try {} catch() {} pour gérer les erreurs
- Se connecter grâce à la méthode getConnection() appliquée à l'objet datasource
- Stocker le résultat dans l'objet cnx
- Déclarer une constante private static final et de type String GET_SUBSCRIBERS 
contenant la requête SQL qui permet d'afficher la liste des abonnés
- Préparer sa requête en déclarant un objet PreparedStatement stmt
- Stocker à l'intérieur le résultat de la méthode preparedStatement() qui prendra en paramètre la constante GET_SUBSCRIBERS et qu'on appliquera sur l'objet cnx
Exécuter la requête grâce à la méthode executeQuery() appliqué sur l'objet stmt
Stocker le résultat dans un objet Resulset rs
Appliquer une boucle while avec la condition suivante :
tant que ce résultat rs rencontre une ligne ds la table grâce à la méthode next()
Appliquer la méthode add à la variable list et lui passer en paramètre un new Subscriber() qui prendra lui-même en paramètre le résultat rs auquel on applique tous les getters qui prennent en paramètre toutes les propriétés de l'objet Subscriber
- Fermer la connexion
- Retourner la liste 

### Exercice 6 : JDBC ###
Paramétrer la connexion à la bdd :

- Ajouter les dépendances suivantes -> MySQL Driver, Spring Data JDBC
Rappel : Ajouter une dépendance en modifiant pom.xml -> Clic Droit / Add Starters ...
- Modifier le fichier application.properties (jdbc, host, port, dbname, username, password)
# configure MySQL database
spring.datasource.driverClassName = com.mysql.cj.jdbc.Driver
spring.datasource.url = jdbc:mysql://localhost:3306/dbname
spring.datasource.username = username
spring.datasource.password = password

### Exercice 7 : CONTROLLER ###
Créer un package controller
A l'intérieur, créer un fichier SubscriberController.java
Précéder la classe de l'annotation @Controller
et de l'annotation @RequestMapping("/subscribers")
Injecter l'interface SubscriberDAO
Créer une route /get grâce à l'annotation @GetMapping()
Implémenter la méthode getAllSubscribers() qui retourne un String et qui prend en paramètre l'objet Model model
Grâce à la méthode addAttribute() appliquée au model, passer 2 paramètres :
- la variable "subscribers"
- la méthode findAll() appliquée à l'objet subscriberDAO
Grâce à la méthode addAttribute() appliquée au model, passer 2 paramètres :
- "newsubscriber"
- new Subscriber()
Retourner le nom de la page html "index"

### Exercice 8 : MOCKUPS ###
Créer des maquettes pour les vues : 
- liste des subscribers
- ajout d'un subscriber
- détail d'un subscriber
- modification d'un subscriber
- suppression d'un subscriber

### Exercice 9 : VIEW ###
Ajouter la dépendance Thymeleaf
Dans le packages templates, créer un fichier index.html
Rappel : Raccourci Emmet (! + Entrée -> génère la structure d'une page HTML type)
Ajouter cette attribut dans la balise html :
xmlns:th="http://www.thymeleaf.org"
Créer un tableau en HTML pour afficher la liste des subscribers
Créer une boucle en utilisant l'attribut th:each 
Utiliser l'attribut th:text pour afficher le contenu des éléments

### Exercice 10 : BOOTSTRAP ###
Installer Bootstrap et mettre en forme la page

### Exercice 11 : FONT AWESOME ###
Installer Font Awesome et ajouter 4 icons sur les boutons (plus, eye, edit, trash-alt)

### Exercice 12 : DAO - MVC ###
Implémenter la méthode add(Subscriber subscriber) qui retourne un int
Modifier la couche DAO : SubscriberDAO + SubscriberDAOImpl
Modifier le contrôleur SubscriberController :
- Créer la route /new pour afficher la vue add.html
- Créer la route /add pour ajouter
Créer la vue add.html (pour le formulaire utiliser :
th:action="@{/add}" 
th:object="${subscriber}")
th:field="*{firstName}"
Modifier le bouton CREATE sur la vue index.html

### Exercice 13 : DAO - MVC ###
Implémenter la méthode getSubscriberById(Integer subscriberId) qui retourne un Subscriber
Modifier la couche DAO : SubscriberDAO + SubscriberDAOImpl
Modifier le contrôleur SubscriberController :
- Créer la route /get/{id} pour afficher la vue get.html
Créer la vue get.html (afficher les données dans un tableau)
Modifier le bouton READ sur la vue index.html

### Exercice 14 : DAO - MVC ###
Implémenter la méthode updateSubscriberById(String newFname, String newLname, String newEmail, Integer subscriberId) qui ne retourne rien
Modifier la couche DAO : SubscriberDAO + SubscriberDAOImpl
Modifier le contrôleur SubscriberController :
- Créer la route /edit/{id} pour afficher la vue edit.html
Créer la vue edit.html (afficher les données dans un tableau)
Modifier le bouton UPDATE sur la vue index.html

### Exercice 15 : DAO - MVC ###
Implémenter la méthode deleteSubscriberById(Integer subscriberId) qui ne retourne rien
Modifier la couche DAO : SubscriberDAO + SubscriberDAOImpl
Modifier le contrôleur SubscriberController :
- Créer la route /delete/{id}
Modifier le bouton DELETE sur la vue index.html